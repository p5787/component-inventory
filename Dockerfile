FROM alpine:latest

ARG HostName

VOLUME [ "/data" ]

RUN apk update && \
    apk add py-pip && \
    apk add postgresql

RUN pip3 install flask psycopg

ADD ServerFile app

ENV FLASK_APP app/main.py

CMD flask run --host 0.0.0.0 >> /data/flask.log

EXPOSE 5000/tcp