.. Gestock documentation master file, created by
   sphinx-quickstart on Wed Feb 16 14:36:36 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Gestock : gestion de stock de composant électronique
====================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   application
   database
   integration
   UI
   docker

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
