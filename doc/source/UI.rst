Interface untilisateur
======================

Grille de mise en page
----------------------
Nous utilisons `masonery` un type de mise en page particulier proposé par certain navigateur.
Il permet d'avoir une mise en page compacte et dynamique sans avoir à réaliser la grille nous même.

C'est une fonctionalité de CSS qui est encore en cours de paufinement par les instances de normalisation.

Pour l'activé, il faut utilisé `firefox` et activé l'option `layout.css.grid-template-masonry-value.enabled`.
Pour les autres navigateurs, l'option n'est pas disponible.