DROP TABLE IF EXISTS Commentaire;
DROP TABLE IF EXISTS AttributionLabel;
DROP TABLE IF EXISTS Composant;
DROP TABLE IF EXISTS Sachet;
DROP TABLE IF EXISTS Reference_constructeur;
DROP TABLE IF EXISTS Boite;
DROP TABLE IF EXISTS Reference_fournisseur;
DROP TABLE IF EXISTS Fournisseur;
DROP TABLE IF EXISTS Label;

CREATE TABLE Fournisseur (
    id SERIAL PRIMARY KEY,
    nom text NOT NULL
);
CREATE TABLE Reference_fournisseur(
    id SERIAL PRIMARY KEY,
    id_fournisseur INTEGER REFERENCES Fournisseur(id),
    code TEXT,
    prix MONEY
);
CREATE TABLE Boite(id SERIAL PRIMARY KEY, nom TEXT);
CREATE TABLE Reference_constructeur(id SERIAL PRIMARY KEY, code TEXT);
CREATE TABLE Sachet(
    id SERIAL PRIMARY KEY,
    nom TEXT,
    id_boite INTEGER REFERENCES Boite(id)
);
CREATE TABLE Composant(
    id SERIAL PRIMARY KEY,
    designation TEXT,
    nombre INTEGER CHECK(nombre >= 0),
    id_sachet INTEGER REFERENCES Sachet(id),
    id_ref_constructeur INTEGER REFERENCES Reference_constructeur(id),
    id_ref_fournisseur INTEGER REFERENCES Reference_fournisseur(id)
);
CREATE TABLE Commentaire(
    id SERIAL PRIMARY KEY,
    id_composant INTEGER REFERENCES Composant(id),
    texte TEXT,
    date_commentaire TIMESTAMP
);
CREATE TABLE Label(
    id SERIAL PRIMARY KEY,
    nom TEXT,
    couleur VARCHAR(8) -- notation hexa RGBA
);
CREATE TABLE AttributionLabel(
    id_label INTEGER NOT NULL REFERENCES Label(id),
    id_composant INTEGER NOT NULL REFERENCES Composant(id),
    PRIMARY KEY (id_label, id_composant)
);
