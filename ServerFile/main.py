import flask
import psycopg

import config
import database
import dumy
import menu


app = flask.Flask(__name__)


@app.route("/")
def menuPrincipal():
    """Generation de la page d'acceuil du site.

    Returns:
        html: code de la page.
    """
    return flask.render_template("main.html.jinja", **{"title": "Page principale", "elemeMenu": menu.liensDefaut})


@app.route("/ping")
def ping():
    return "pong"


@app.route("/inventaire-dummy")
def inventaire_dummy():
    return flask.render_template("inventaire.html.jinja", **{"title": "Page inventaire", "elemeMenu": menu.liensDefaut, "elemInventaire": dumy.dumyComposant})


@app.route("/inventaire")
def inventaire_full():
    """Generation de la page d'inventaire complet.

    Returns:
        html: code de la page.
    """
    with database.getConnection() as connection:
        with connection.cursor() as curs:
            comps = database.Composant.getFromDbKeyWord("", curs)
            for comp in comps:
                comp.labels = comp.getLabel(curs)
    return flask.render_template("inventaire.html.jinja", **{"title": "Page inventaire", "elemeMenu": menu.liensDefaut, "elemInventaire": comps})


@app.route("/insertion", methods=["GET", "POST"])
def insertion():
    if flask.request.method == 'POST':
        formulaire = flask.request.form
        try:
            post_description = formulaire['description']
            post_nombre = int(formulaire['nombre'])
            post_emplacement = int(formulaire['emplacement'])
            post_fournisseur = int(formulaire['fournisseur'])
            post_ref_fournisseur = formulaire['ref_four']
            post_ref_constructeur = formulaire['ref_fab']
            labels = formulaire.getlist('label')
        except (KeyError, ValueError):
            flask.abort(400)
        with database.getConnection() as connection:
            with connection.cursor() as curs:
                database.Composant.ajout(curs, post_description, post_nombre, post_emplacement,
                                         post_fournisseur, post_ref_fournisseur, post_ref_constructeur, labels)

    with database.getConnection() as connection:
        with connection.cursor() as curs:
            fournisseurs = database.Fournisseur.getAll(curs)
            emplacements = database.Emplacement.getAll(curs)
            labels = database.Label.getAll(curs)

    return flask.render_template("insertion.html.jinja", **{"title": "Ajout de composants", "elemeMenu": menu.liensDefaut, "fournisseurs": fournisseurs, "emplacements": emplacements, "labels": labels})


@app.route("/label", methods=["GET", "POST"])
def admin_label():
    if flask.request.method == 'POST':
        formulaire = flask.request.form
        # try:
        if formulaire['type_request'] == "ajout":
            nom = formulaire['nom']
            couleur = formulaire['couleur']
            with psycopg.connect(dbname=config.dbname, user=config.dbuser, password=config.dbpassword) as connection:
                with connection.cursor() as curs:
                    database.Label.ajout(curs, nom, couleur)
        # except (KeyError, ValueError):
        #    flask.abort(400)

    return flask.render_template("admin_label.html.jinja", **{"title": "Ajout de composants", "elemeMenu": menu.liensDefaut})


@app.route("/recherche", methods=["POST"])
def recherche():
    """Generation de la page de résultat de recherche.

    Post:
    - ``recherche`` : mot clef de la recherche.

    Returns:
        html: code la page.
    """
    try:
        motsClefs = flask.request.form['recherche']
        if motsClefs == "":
            flask.abort(400)
    except KeyError:
        flask.abort(400)
    with database.getConnection() as connection:
        with connection.cursor() as curs:
            comps = database.Composant.getFromDbKeyWord(motsClefs, curs)
    return flask.render_template("inventaire.html.jinja", **{"title": "Résultat de recherche", "elemeMenu": menu.liensDefaut, "elemInventaire": comps})


if __name__ == "__main__":
    pass
