#! bin/python3
import csv

import psycopg

import config
import database

composants = []
fournisseur = []
sachet = {}
prix = {}
boite = []
ref_f = {}
ref_c = []

id = 0

with open("old_db.csv", "r") as file:
    reader = csv.reader(file)
    for line in reader:
        try:
            line = line[0].split(";")
            line = [elem.strip() for elem in line]
            composants.append(database.Composant(
                line[3],
                int(line[4]),
                line[8]))
            if line[7] not in boite:
                boite.append(line[7])
            if line[8] not in sachet:
                sachet[line[8]] = line[7]
            line[1] = line[1].upper()
            if line[1] not in fournisseur:
                fournisseur.append(line[1])
            if line[0]:
                if line[0] not in ref_f:
                    ref_f[line[0]] = line[1]
                composants[-1].ref_f = line[0]
            else:
                composants[-1].ref_f = None
            if line[2]:
                if line[2] not in ref_c:
                    ref_c.append(line[2])
                composants[-1].ref_c = line[2]
            else:
                composants[-1].ref_c = None
        except BaseException:
            pass


with psycopg.connect(host=config.dbhost, port=config.dbport, dbname=config.dbname, user=config.dbuser, password=config.dbpassword) as connection:
    with connection.cursor() as curs:
        for elem in fournisseur:
            curs.execute("""
            INSERT INTO fournisseur (nom)
            VALUES (%s);
            """, (elem,))
        for elem in ref_c:
            curs.execute("""
            INSERT INTO reference_constructeur (code)
            VALUES ( %s );
            """,(elem,))
        for elem in boite:
            curs.execute("""
            INSERT INTO boite (nom)
            VALUES ( %s );
            """,(elem,))
        for elem in ref_f:
            curs.execute("""
            INSERT INTO reference_fournisseur (id_fournisseur, code)
            VALUES (
                (SELECT id FROM fournisseur WHERE nom = %s),
                %s
              );
            """, (ref_f[elem], elem))
        for elem in sachet:
            curs.execute("""
            INSERT INTO sachet (id_boite, nom)
            VALUES (
                (SELECT id FROM boite WHERE nom = %s),
                %s
              );
            """, (sachet[elem], elem))
        for composant in composants:
            curs.execute("""
            INSERT INTO composant (
                designation,
                nombre,
                id_ref_fournisseur,
                id_ref_constructeur,
                id_sachet
              )
            VALUES (
                %s,
                %s,
                (SELECT id FROM reference_fournisseur WHERE code = %s),
                (SELECT id FROM reference_constructeur WHERE code = %s),
                (SELECT id FROM sachet WHERE nom = %s)
              );
            """, (composant.description, composant.nombre,  composant.ref_f, composant.ref_c, composant.emplacement))
