class MenuElement():
    def __init__(self, nom: str, lien: str) -> None:
        self._nom = nom
        self._lien = lien

    @property
    def nom(self):
        return self._nom

    @property
    def lien(self):
        return self._lien


liensDefaut = [MenuElement('Accueil','/'),
               MenuElement('Inventaire', "inventaire"),
               MenuElement('Insertion', "insertion"),
               MenuElement('Labels', "label"),
               ]
