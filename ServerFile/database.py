import psycopg

import config


def getConnection(host=config.dbhost):
    return psycopg.connect(host=host, port=config.dbport, dbname=config.dbname, user=config.dbuser, password=config.dbpassword)


class Composant():
    @staticmethod
    def getFromDbKeyWord(keyWord: str, cursor: psycopg.Cursor):
        cursor.execute("""
        SELECT c.id, c.designation, c.nombre, CONCAT(b.nom, s.nom)
        FROM composant AS c
        JOIN sachet AS s ON c.id_sachet=s.id
        JOIN boite AS b ON s.id_boite=b.id
        WHERE (c.designation ILIKE %s);
        """, ("%"+keyWord + "%",))
        # TODO protection injection
        return [Composant(record[1], record[2], record[3], record[0]) for record in cursor]

    @staticmethod
    def ajout(cursor: psycopg.Cursor, description: str, nombre: int, id_emplacement: int, id_fournisseur: int, ref_fournisseur: str, ref_constructeur: str, labels=[], prix: float = None):
        """Rajout d'un composant dans la DB

        Args:
            cursor (psycopg.Cursor): curseur de la DB.
            description (str): Description du composant
            nombre (int): Nombre de composant à ajouter
            emplacement (int): Id de l'emplacement du composant
            fournisseur (int): Id du fournisseur du composant
            ref_fournisseur (str): Code de la référence fournisseur
            ref_constructeur (str): Code de la référence constructeur
            prix (float, optional): Prix du composant. Defaults to None.
        """

        id_ref_fournisseur = RefFournisseur.getByName(cursor, ref_fournisseur, id_fournisseur, True)
        id_ref_constructeur = RefConstructeur.getByName(cursor, ref_constructeur, True)

        # Insersion du composant dans la base
        cursor.execute("""
        INSERT INTO composant (
            designation,
            nombre,
            id_sachet,
            id_ref_constructeur,
            id_ref_fournisseur
          )
        VALUES (
            %s,
            %s,
            %s,
            %s,
            %s)
        RETURNING id;
        """, (description, nombre, id_emplacement, id_ref_fournisseur, id_ref_constructeur))
        comp_id = cursor.fetchall()[-1][0]

        for label in labels:
            cursor.execute("""
            INSERT INTO attributionlabel (id_label, id_composant)
            VALUES (
                %s,
                %s
              );
            """, (label, comp_id))

    def __init__(self, description: str, nombre: int, emplacement: str, id=None) -> None:
        self._id = id
        self._desc = description
        self._nb = nombre
        self._empla = emplacement

    @ property
    def id(self) -> int:
        return self._id

    @ property
    def description(self) -> str:
        return self._desc

    @ property
    def nombre(self) -> int:
        return self._nb

    @ property
    def emplacement(self) -> str:
        return self._empla

    def getLabel(self, cursor: psycopg.Cursor):
        cursor.execute("""
        SELECT l.id, l.nom, l.couleur FROM
            label AS l
            JOIN attributionlabel AS c ON (l.id = c.id_label)
            WHERE (c.id_composant = %s)
        """, (self.id,))
        return [Label(*elem) for elem in cursor]


class Emplacement():
    @staticmethod
    def getAll(cursor: psycopg.Cursor):
        cursor.execute(
            "SELECT s.id, b.nom, s.nom FROM boite AS b JOIN sachet AS s ON b.id=s.id_boite;")
        return [Fournisseur(record[1]+record[2], record[0]) for record in cursor]

    def __init__(self, nom: str, id: int = None):
        self._id = id
        self._nom = nom

    @property
    def id(self):
        return self._id

    @property
    def nom(self):
        return self._nom


class RefFournisseur():

    @staticmethod
    def getByName(cursor: psycopg.Cursor, ref: str, id_fournisseur: int, addIfMissing: bool = False):
        if ref == "":
            return None
        cursor.execute("""
        SELECT COUNT(id), id
            FROM reference_fournisseur
            WHERE
                ((id_fournisseur=%s)
            AND
                (code=%s));
        """, (id_fournisseur, ref))
        nbLigne, id = cursor.fetchone()
        if nbLigne > 1:
            raise SchemaError("Deux références fournisseur de même code")
        elif (nbLigne == 0) and addIfMissing:
            cursor.execute("""
            INSERT INTO reference_fournisseur (id_fournisseur, code)
            VALUES (
                %s,
                %s
                    )
            RETURNING id;
            """, (ref, id_fournisseur))
            id = cursor.fetchone()[0]
        return id


class RefConstructeur():

    @staticmethod
    def getByName(cursor: psycopg.Cursor, ref: str, addIfMissing: bool = False):
        if ref == "":
            return None
        cursor.execute("""
        SELECT COUNT(id), id
            FROM reference_constructeur
            WHERE
                (code=%s);
        """, (ref,))
        nbLigne, id = cursor.fetchone()
        if nbLigne > 1:
            raise SchemaError("Deux références constructeur de même code")
        elif (nbLigne == 0) and addIfMissing:
            cursor.execute("""
            INSERT INTO reference_constructeur (code)
            VALUES (
                %s
              )
            RETURNIG id;
            """, (ref))
            id = cursor.fetchone()[0]
        return id


class Fournisseur():
    @staticmethod
    def getAll(cursor: psycopg.Cursor):
        cursor.execute("SELECT id, nom FROM Fournisseur;")
        return [Fournisseur(record[1], record[0]) for record in cursor]

    def __init__(self, nom: str, id: int = None):
        self._id = id
        self._nom = nom

    @property
    def id(self):
        return self._id

    @property
    def nom(self):
        return self._nom


class Label():
    @staticmethod
    def ajout(cursor: psycopg.Cursor, nom: str, couleur: str):
        cursor.execute(""" 
        INSERT INTO label (nom, couleur)
        VALUES (%s, %s)
        """, (nom, couleur))

    @staticmethod
    def getAll(curs: psycopg.Cursor):
        curs.execute("""
        SELECT id, nom, couleur FROM label;
        """)
        return [Label(int(record[0]), record[1], record[2]) for record in curs]

    def __init__(self, id: int, nom: str, couleur: str) -> None:
        self.id = id
        self.nom = nom
        self.couleur = couleur


class NonExistanceExeption(BaseException):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class SchemaError(BaseException):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)
